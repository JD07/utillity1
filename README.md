# Utillity1



Requirement

If any of the columns is missing in the source data while comparing with target metadata, 
those columns should be added to the dataframe with null before writing to DB

For eg

Source data
Empid,name

TargetData
Empid,name,deptid

Code should be capable of adding deptid to the transformed dataframe with null value
